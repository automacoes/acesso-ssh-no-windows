# acesso ssh no windows

A necessidade deste script destina-se à acessar remotamente o Windows pelo terminal do Linux via SSH, efetuando automações com comandos remotos.

Atualmente já existe uma chave SSH no GitLab, não sendo necessário a criação de uma.

**1. Efetue o clone deste repositório no disco C:\ do servidor que precisa do acesso e execute a linha abaixo:**
```
C:\OpenSSH-Win-Config.ps1 -Install -Download -PublicKeyOnly -KeyPath "C:\key.pub"
```

**2. Acessar com o servidor do GitLab 172.20.0.85 e executar o seguinte comando:**
```
ssh -i /home/gitlab/.ssh/automacao administrator@
```

O script deste repositório se encarrega de instalar, configurar e ativar o SSH no Windows.

> IMPORTANTE
> **Caso não tiver uma chave SSH ainda**, antes de executar o script _OpenSSH-Win-Config.ps1_, efetue as ações abaixo:

**3. Crie a chave SSH no servidor Linux, caso o diretório não exista, deve ser criado.**
```
cd ~/.ssh
ssh-keygen
```
O comando acima, gera duas chaves, copie o conteúdo da chave pública com final .pub 

**4. Execute o Script _OpenSSH-Win-Config.ps1_ no Windows:**
```
C:\OpenSSH-Win-Config.ps1 -Install -Download -PublicKeyOnly -KeyPath "C:\key.pub"
```

**5. Ao finalizar o processo, basta acessar o servidor remoto Windows pelo linux**
```
ssh -i /home/gitlab/.ssh/automacao administrator@
```

**6. Caso desejar executar apenas um comando sem abrir a sessão, deve ser executado após o comando de conexão ssh.**
```
ssh -i /home/gitlab/.ssh/automacao administrator@ date
```
